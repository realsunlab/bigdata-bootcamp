---
layout: SpecialPage
---
# About Us

<!--[sunlab-team](images/avatar/aboutus.jpg "Sunlab team")-->

![sunlab-team](images/avatar/aboutus.jpg "Sunlab team")

## Remote Office Hour

We will host multiple remote office hour (60 mins) through Zoom weekly. The schedule can be find on this [Google Calendar](https://calendar.google.com/calendar/u/0?cid=ZWVhYzU4ZmY4bDh0ajcxZzk5N2djNWdka2NAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ). Office hour is used for Q&A only and no instruction will be given.

| Photo| Name| Location or Web Link |
| :-------------: | :-------------: | --------------------------------------------------------------------------------------|
|![minipic](images/avatar/Jimeng.png)   |  Jimeng Sun, instructor, jsun@cc.gatech.edu    |       Request by email         |
|![minipic](images/avatar/Yuzheng.jpeg) | Yuzheng Liu ,  Head TA, liuyz@gatech.edu | <https://gatech.zoom.us/j/9400574255?pwd=Um1SSUV3dVhkVDFoT0VLOHh5VTluZz09> |
|![minipic](images/avatar/Wenjin_Song.JPG) | Wenjin Song, TA, wsong93@gatech.edu   | <https://gatech.zoom.us/j/7874821797?pwd=R2F6UmFpUmV1bXdZRG43azA0dS8rZz09> |
|![minipic](images/avatar/Bojun_2.JPG) | Bojun Li, TA, bojun.li@gatech.edu   |  <https://gatech.zoom.us/j/4124121997> |
|![minipic](images/avatar/Ruoyu.JPG) | Ruoyu Wang, TA, rwang764@gatech.edu   |  <https://gatech.zoom.us/j/5680075320?pwd=elZyaE5nNHltS0NhNEVIT3l1dTJJdz09> |
|![minipic](images/avatar/zundong.JPG) | Zundong Wu, TA, zwu366@gatech.edu   |  <https://gatech.zoom.us/j/9858861386?pwd=MzRCYThiRGpLOVorcFMzT0tFZGJDdz09> |

