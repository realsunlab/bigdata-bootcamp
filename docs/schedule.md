---
layout: SpecialPage
---
# Course Syllabus

[[toc]]

## Overview
Students should watch Canvas/Ed Lessons course videos according to the following schedule. It is **recommended** for students to do lab sessions on the schedule by yourself **as early as possible** since some of homework may cover the lab materials scheduled later than the homework.
For the online video lectures, CS/CSE students should go to Udacity or Canvas to access to the sources.
## Schedule

| Week # | Dates     |  Video lessons                       | Lab                              | Deliverable & Due (EDT)         | 
|--------|-----------|-----------------------------------|-------------------------------------|----------------------------------|
| 1      | Jan 6-10  |  [1. Intro to Big Data Analytics], [2. Course Overview]     |                                     |                 | 
| 2      | Jan 13-17 |  [3. Predictive Modeling]       |   [Hadoop & HDFS Basics]                                   |    HW1 Due (Jan 20)                          | 
| 3      | Jan 20-24 |  [4.MapReduce]& [HBase]                  |   [Hadoop Pig & Hive]                                   |                                              | 
| 4      | Jan 27-31 |  [5.Classification evaluation metrics], [6.Classification ensemble methods] |                    |   HW2 Due (Feb 3)                             | 
| 5      | Feb 3-7  |  [7. Phenotyping], [8. Clustering]     |  [Scala Basic], [Spark Basic], [Spark SQL]             |      Project Group Formation (Feb 10)          | 
| 6      | Feb 10-14 |  [9. Spark]                            |   [Spark Application] & [Spark MLlib]                   |    Project Proposal Due (Feb 17)                   | 
| 7      | Feb 17-21 |  [10. Medical ontology]                 |  [NLP Lab]                                                |                                                  | 
| 8      | Feb 24-28 |  [11. Graph analysis]                  | [Spark GraphX]                                            |     HW3 Due (Mar 3)                             | 
| 9      | Mar 3-7  |  [12. Dimensionality Reduction], [13. Patient similairty], [14. CNN]  |   [Deep Learning Lab]   |                                                   | 
| 10     | Mar 10-14 |   [15. DNN], [16. RNN]               |                                                           |                   HW4 Due (Mar 17)                | 
| 11     | Mar 17-21 |   Project Discussion              |                                                          |                                                      |
| 12     | Mar 24-28 |  Project Discussion                   |                                                             |           Project Draft Due (Mar 31)                | 
| 13     | Mar 31-Apr 4 |  Project Discussion              |                                                                |         Final Exam (Apr 13-14)                    | 
| 14     | Apr 7-11 |  Project Discussion               |                                                              |                                                   | 
| 15     | Apr 14-18 |  Project Discussion               |                                                           |           Final Project Due (Apr 21)                | 
| 16     | Apr 21-25 |   Final Grading                        |                                                      |                                                |  


## Previous Guest Lectures

See [RESOURCE](/resource.html) section.
