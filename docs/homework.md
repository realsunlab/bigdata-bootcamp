---
layout: SpecialPage
# sidebarDepth: 2
---
# Homework

<!-- subtitle: Homwork description and submission -->

## Important dates

- Jan 20: Homework 1 due
- Feb 3: Homework 2 due
- Mar 3: Homework 3 due
- Mar 17: Homework 4 due

## Release

Homework of this class will be distributed through [Canvas](http://gatech.instructure.com/). All homework material can only be used within class.

## Discussion

For questions regarding homework, we encourage you to use Ed Discussion to raise discussion and we will answer ASAP. If necessary you can email [instructor and TAs](/contact.html). Remote TA office hour will be hosted through meeting software.<!--For on-campus students, we will schedule TA office hours.-->

## Submission
We will use Canvas and Gradescope for assignments/project submission this semester, please check more details in each assignment/project description, failure to follow guidelines will yield nothing finally.
As a practical class, we will have a lot of **programming questions**. Please strictly follow submission instructions in each homework, especially naming and structure of submission. It's your responsibility to make sure your submission is compilable and runnable in standard teaching environment with provided code skeleton. Non-runnable code will directly lead to 0 score. You are not allowed to change any existing function or class names from provided skeleton code unless got permission from instructor or TA. Otherwise, your submission may fail our tests and got penalty in your score.

## COLLABORATION & GROUP WORK

Homework assignments are strictly individual efforts, while final projects are done by small groups. You can discuss high level concepts regarding to lectures or homework on the forum but you shouldn't share your own (or others') solution and code with other students through any means, and we will use anti-cheating software to avoid cheating.

## EXTENSIONS, LATE ASSIGNMENTS, & RE-SCHEDULED/MISSED EXAMS

Each student is allowed 2 days of late submission in total to be used for HOMEWORK only. For each homework, we will count the late day independently (even 1 hour late will be counted as 1 day, and 25 hours counted as 2 days). Once you have used up your late days, late assignments will be penalized at 8 pts per day, assignments more than 3 days late will not be accepted (The deadlines are on Mondays 8:00 AM EST, and no submission will be accepted after the following Thursdays 8:00 AM EST). Late day penaly of each homework will be accumulated and evaluated at the end of the term. We don't consider ANY other homework extension excuses except healthcare emergency incident (doctor's proof is required).